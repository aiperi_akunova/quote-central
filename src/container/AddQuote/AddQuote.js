import React, {useEffect, useState} from 'react';
import './AddQuote.css';
import axios from "axios";

const AddQuote = ({history}) => {


    const [quotes, setQuotes] = useState([
        {
            author: '',
            category: '',
            text: '',
        }
    ]);

    const toSubmitQuote = (e) => {
        e.preventDefault();
        const fetchData = async () => {
            const response = await axios.post('https://quotes-central-40c7e-default-rtdb.firebaseio.com/quotes.json', quotes);
            history.replace('/');
        }
        fetchData().catch(e => console.error(e));
    }

    const onInputChange = (e)=>{
        const {name,value} = e.target;
        setQuotes(prev=>({
            ...prev,
            [name]: value
        }))
    }

    return (
        <div>
            <h3>Submit new quote</h3>

            <form className='add-quote-form' onSubmit={toSubmitQuote}>
                <label>
                    Category:
                    <select
                        className='category-select'
                        value={quotes.category}
                        name='category'
                        onChange={onInputChange}
                    >
                        <option>Sayings</option>
                        <option>Positive</option>
                        <option>Motivational</option>
                        <option>Humor</option>
                        <option>Wisdom</option>
                    </select>
                </label>
                <label>
                    Author:
                    <input
                        type='text'
                        className='author-input'
                        value={quotes.author}
                        name = 'author'
                        onChange={onInputChange}
                        autoComplete='off'
                    />
                </label>

                <label>
                    Quote Text:
                    <textarea
                        className='text'
                        value={quotes.text}
                        name= 'text'
                        onChange={onInputChange}
                        autoComplete='off'
                    >

                    </textarea>
                </label>

                <button className='submit-btn'>Submit</button>
            </form>
        </div>
    );
};

export default AddQuote;