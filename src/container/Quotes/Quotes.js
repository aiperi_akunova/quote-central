import React, {useCallback, useEffect, useState} from 'react';
import './Quotes.css';
import axios from "axios";
import QuoteShowForm from "../../components/QuoteShowForm/QuoteShowForm";

const Quotes = ({match, history}) => {

    const [allQuotes, setAllQuotes] = useState([]);

    useEffect(()=>{
        const fetchData = async ()=>{

            if(match.params.id){
                const myPath = match.params.id;
                const myPathCapitalize = myPath.charAt(0).toUpperCase() + myPath.slice(1);
                const response = await axios.get('https://quotes-central-40c7e-default-rtdb.firebaseio.com/quotes.json?orderBy="category"&equalTo="'+myPathCapitalize+'"');
                const result = response.data;
                setAllQuotes(result);
            } else {
                const response = await axios.get('https://quotes-central-40c7e-default-rtdb.firebaseio.com/quotes.json');
                const result = response.data;
                setAllQuotes(result);
            }

        }
        fetchData().catch(e => console.error(e));


    },[match.params.id]);

    const requestAgain = useCallback(async()=>{
            const response = await axios.get('https://quotes-central-40c7e-default-rtdb.firebaseio.com/quotes.json');
            const result = response.data;
            setAllQuotes(result);
    }, [match.params.id]);



    const onDelete = (id) =>{
        const fetchData = async ()=>{
            await axios.delete('https://quotes-central-40c7e-default-rtdb.firebaseio.com/quotes/'+id+'.json');
            await  requestAgain();
        }
        fetchData().catch(console.error)

    }



    return (
        <div>
            {match.params.id}
            {Object.keys(allQuotes).map(q=>(
               <QuoteShowForm
                   text = {allQuotes[q].text}
                   author = {allQuotes[q].author}
                   id = {q}
                   onDelete = {()=>onDelete(q)}
                />
            ))}
        </div>
    );
};

export default Quotes;