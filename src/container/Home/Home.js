import React from 'react';
import './Home.css';
import {Categories} from "../../Categories";
import Quotes from "../Quotes/Quotes";
import {NavLink, Route, Switch} from "react-router-dom";


const Home = ({match}) => {
    const category = Categories;
    return (
        <div className='home'>
            <div className='category-list'>
                <ul>
                    {category.map(c=>(
                        <li><NavLink to={'/quotes/'+c.id}>{c.title}</NavLink> </li>
                    ))}
                </ul>

            </div>

            <div className='quotes'>

                <Switch>
                    <Route path = '/quotes/:id' render ={props=><Quotes {...props}/>} />
                    <Route path = '/' render ={props=><Quotes {...props}/>} />
                </Switch>

            </div>
        </div>
    );
};

export default Home;