import './App.css';
import Header from "./components/Header/Header";
import AddQuote from "./container/AddQuote/AddQuote";
import Quotes from "./container/Quotes/Quotes";
import Home from "./container/Home/Home";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import React from "react";

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Header/>
                <Switch>
                    <Route path = '/' exact component = {Home}/>
                    <Route path = '/submit'  component = {AddQuote}/>
                    <Route path = '/quotes/:id' component = {Quotes} />
                </Switch>
            </BrowserRouter>
        </div>
    );
}

export default App;
