import React, {useCallback, useEffect} from 'react';
import './QuoteShowForm.css';
import axios from "axios";

const QuoteShowForm = props => {

    // const onDelete = e =>{
    //     e.preventDefault();
    //     const fetchData = async ()=>{
    //         await axios.delete('https://quotes-central-40c7e-default-rtdb.firebaseio.com/quotes/'+props.id+'.json');
    //     }
    //     fetchData().catch(console.error)
    //
    // }

    return (
        <div className='one-quote'>
            <div className='quote-info'>
                <p>"{props.text}"</p>
                <p> - <span> {props.author}</span></p>
            </div>
            <div className='buttons'>
                <button className='edit-btn'>Edit </button>
                <button className='delete-btn' onClick={props.onDelete}>Delete </button>
            </div>
        </div>
    );
};

export default QuoteShowForm;