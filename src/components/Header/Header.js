import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <header>
            <div className='logo-box'>
                <h2>Quotes Central</h2>
            </div>
            <div className='links'>
                <NavLink to='/'>Quotes</NavLink>
                <NavLink to='/submit'>Submit new quote</NavLink>
            </div>
        </header>
    );
};

export default Header;