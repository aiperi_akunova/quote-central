
export const Categories = [
    { title: 'Positive', id: 'positive'},
    { title: 'Motivational', id: 'motivational'},
    { title: 'Sayings', id: 'sayings'},
    { title: 'Humor', id: 'humor'},
    { title: 'Wisdom', id: 'wisdom'},
]

